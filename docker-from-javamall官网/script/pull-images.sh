#!/bin/bash
docker pull docker.javamall.com.cn:5001/javashop-infrastructure/portainer:latest
docker pull docker.javamall.com.cn:5001/javashop-infrastructure/consul:latest
docker pull docker.javamall.com.cn:5001/javashop-infrastructure/mariadb-cluster:latest
docker pull docker.javamall.com.cn:5001/javashop-infrastructure/pivotal-rabbitmq-autocluster:3.7-consul
docker pull docker.javamall.com.cn:5001/javashop7/api:2.0
docker pull docker.javamall.com.cn:5001/javashop-infrastructure/openresty:latest
docker pull docker.javamall.com.cn:5001/javashop-infrastructure/es:5.5.0
docker pull docker.javamall.com.cn:5001/javashop7/nodejs-pc:2.0
docker pull docker.javamall.com.cn:5001/javashop7/nodejs-wap:2.0
docker pull docker.javamall.com.cn:5001/javashop7/buyer-pc:1.0
docker pull docker.javamall.com.cn:5001/javashop7/buyer-wap:1.0
docker pull docker.javamall.com.cn:5001/javashop7/manager-seller:1.0
docker pull docker.javamall.com.cn:5001/javashop7/manager-admin:1.0