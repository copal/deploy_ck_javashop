#!/bin/bash

#默认堆大小
Xmx=256m
Xms=256m
Xss=256k

if [ ! -n "$3"  ];then
         echo " java -Xmx$Xmx -Xss$Xss -Dfile.encoding=utf-8  -jar $1 $2"
         java -Xmx$Xmx -Xss$Xss -Dfile.encoding=utf-8  -jar $1 $2
else
	 echo "java $3 -jar $1 $2"
         java $3 -jar $1 $2

fi
